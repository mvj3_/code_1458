[代码] [Java]代码
view source
print?
	package com.example.tablayoutdemo;
	 
	import android.R.integer;
	import android.content.Context;
	import android.util.AttributeSet;
	import android.view.MotionEvent;
	import android.view.View;
	import android.view.ViewGroup;
	import android.widget.BaseAdapter;
	/**
	 * 标签布局，本类需要一个适配器来提供子组件。
	 * <p>可选的切换效果可以选择在本布局中设置background，并在子组件根布局中实现selector达到背景切换的目的。
	 * <p>另外一种可以选择全部做在子组件的selector中。
	 *
	 * <p>对于标签切换，可以使用 {@link com.mzke.gongshang.app.widget.TabLayout.OnTabSelectedListener} 来监听
	 * @author Damet Liu
	 * @see {@link com.mzke.gongshang.app.widget.TabLayout.OnTabSelectedListener}
	 */
	public class TabLayout extends ViewGroup {
	    private BaseAdapter adapter = null;
	    private OnTabSelectedListener onTabSelectedListener = null;
	     
	    public TabLayout(Context context) {
	        this(context, null);
	    }
	     
	    public TabLayout(Context context, AttributeSet attrs) {
	        this(context, attrs, 0);
	    }
	     
	    public TabLayout(Context context, AttributeSet attrs, int defStyle) {
	        super(context, attrs, defStyle);
	    }
	     
	    @Override
	    protected void onLayout(boolean changed, int l, int t, int r, int b) {
	        //以横向布局子组件，为GONE的不占用空间
	        int childLeft = 0;
	        final int childCount = getChildCount();
	        for (int i = 0; i < childCount; i++) {
	            final View childView = getChildAt(i);
	            if (childView.getVisibility() != View.GONE) {
	                final int childWidth = childView.getMeasuredWidth();
	                childView.layout(childLeft, 0, childLeft + childWidth,
	                        childView.getMeasuredHeight());
	                childLeft += childWidth;
	            }
	        }
	    }
	     
	    /**
	     * 获取有效组件的数量，GONE的不计算
	     * @return 有效组件的数量
	     */
	    protected int getValidChildCount() {
	        int result = 0;
	        for (int i = 0; i < getChildCount(); i++) {
	            if (getChildAt(i).getVisibility() != View.GONE) {
	                result++;
            }
	        }
	        return result;
	    }
	     
	    @Override
	    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	         
	        final int widthSize = MeasureSpec.getSize(widthMeasureSpec);
	        final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
	        if (widthMode != MeasureSpec.EXACTLY) {
	            throw new IllegalStateException(
	                    "TabLayout only can run at EXACTLY mode!");
	        }
	         
	        final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
	        if (heightMode != MeasureSpec.EXACTLY) {
	            throw new IllegalStateException(
	                    "TabLayout only can run at EXACTLY mode!");
	        }
	         
	        //为子组件平均分配所有空间
	        final int validCount = getValidChildCount();
	        if (validCount > 0) {
	            widthMeasureSpec = MeasureSpec.makeMeasureSpec(widthSize
	                    / validCount, MeasureSpec.EXACTLY);
	            final int count = getChildCount();
	            for (int i = 0; i < count; i++) {
	                getChildAt(i).measure(widthMeasureSpec, heightMeasureSpec);
	            }
	        }
	    }
	     
	    @Override
	    public boolean onTouchEvent(MotionEvent event) {
	        //当触摸该组件时，将会切换焦点以及通知监听器
	        final int action = event.getAction();
	        switch (action) {
	            case MotionEvent.ACTION_DOWN:
	                final int position = getChildIndex(event);
	                setSelection(position);
	                break;
	        }
	        return super.onTouchEvent(event);
	    }
	    /**
	     * 选择标签
	     *
	     * <p>如果想在初始化时，正确显示默认标签，
	     * 应该在{@link #setAdapter(BaseAdapter)}
	     * 之前调用{@link #setOnTabSelectedListener(OnTabSelectedListener)}
	     */
	    public void setSelection(int position)
	    {
	        getChildAt(position).requestFocus();
	        if(onTabSelectedListener != null)
	            onTabSelectedListener.onTabSelectedListener(position);
	    }
	     
	    /**
	     * 根据触摸位置得到子组件的位置
	     * @return 被触摸的子组件索引
	     */
	    public int getChildIndex(MotionEvent event) {
	        final int x = (int) event.getX();
	        final int valid = getValidChildCount();
	        final int childWidth = getMeasuredWidth() / valid;
	         
	        int index = 0;
	         
	        int left = 0, right = childWidth;
	        for (int i = 0; i < valid; i++, left += childWidth, right += childWidth) {
	            if (x >= left && x <= right) {
	                return i;
	            }
	        }
	        return index;
	    }
	     
	    /**
	     * 为组件提供适配器，该方法将清楚所有的已存在的子组件。
	     * 并且会为每个子组件的根布局开启触摸焦点，如果子组件内
	     * 含有可以获取焦点的组件，会导致本组件切换效果失效。
	     */
	    public void setAdapter(BaseAdapter adapter) {
	        this.adapter = adapter;
	         
	        removeAllViews();
	        for (int i = 0; i < adapter.getCount(); i++) {
	            final View child = adapter.getView(i, null, this);
	            child.setFocusable(true);
	            child.setFocusableInTouchMode(true);
	            addView(child);
	        }
	        setSelection(0);
	    }
	     
	    /**
	     * 获取该组件的适配器
	     * @return
	     */
	    public BaseAdapter getAdapter() {
	        return this.adapter;
	    }
	     
	    /**
	     * 为组件设置切换时的监听器
	     */
	    public void setOnTabSelectedListener(OnTabSelectedListener onTabSelectedListener)
	    {
	        this.onTabSelectedListener = onTabSelectedListener;
	    }
	     
	    /**
	     * 获取选择tab的监听器
	     * @return
	     */
	    public OnTabSelectedListener getOnTabSelectedListener()
	    {
	        return this.onTabSelectedListener;
	    }
	     
	    /**
	     * {@link #com.mzke.gongshang.app.widget.TabLayout}
	     * 的选择监听器，该监听器事件会在切换标签时被触发
	     * @author Damet Liu
	     */
	    public static abstract interface OnTabSelectedListener
	    {  
	        public void onTabSelectedListener(int position);
	    }
	}